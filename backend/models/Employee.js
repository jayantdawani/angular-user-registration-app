const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Employee = new Schema({
   name: {
      type: String, 
   },
   email: {
      type: String
   },
   designation: {
      type: String
   },
   number: {
      type: Number
   },
   password: {
      type: String
   },
   status: {
      type: String,
      default: '0'
   }
}, {
   collection: 'employees'
})

module.exports = mongoose.model('employees', Employee)